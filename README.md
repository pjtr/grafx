# README #

Grafx is a small and simple JavaFX graph visualisation library, where the word "graph" refers to a collection of vertices and edges and not a chart.

## Features ##

* The layout of the graph can be manually changed by draggin nodes.

That's all. As mentioned before, it's a simple library. But fully functional and bugfree!

## License ##

LGPL, which you can use it anywhere, but any modifications are automatically GPL and must be published.

## Building and running

It's plain Java, no dependencies whatsoever. Just get the source, import it in your favorite IDE and hit the run button. Requires Java 8.
