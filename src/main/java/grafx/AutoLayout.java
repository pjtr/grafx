package grafx;

import javafx.geometry.Point2D;

import java.util.List;

public interface AutoLayout<V> {

    List<Point2D> computePositions(List<Vertex<V>> vertices);
}
