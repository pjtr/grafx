package grafx;

import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// TODO: kan de V parameter vervangen door een wildcard?
public class SimpleCircleLayout<V> implements AutoLayout<V> {

    public List<Point2D> computePositions(List<Vertex<V>> vertices) {
        Collections.shuffle(vertices);
        int count = vertices.size();
        int circumference = count * 200;
        double radius = circumference / (2 * Math.PI);
        List<Point2D> positions = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            double angle = 2 * Math.PI * i / count;
            positions.add(new Point2D(100.0 + radius + Math.sin(angle) * radius, 100 + radius + Math.cos(angle) * radius));
        }
        return positions;
    }

}
