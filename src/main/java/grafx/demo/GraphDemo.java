package grafx.demo;

import grafx.GraphModel;
import grafx.GraphPane;
import grafx.GridLayout;
import grafx.SimpleCircleLayout;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GraphDemo extends Application {

    private List<GraphModel> models = new ArrayList<>();
    private GraphModel model = null;
    private int modelIndex = 0;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void init() {
        try {
            for (String tgf: new String[] {
                    "A\nB\nC\nD\nE\nF\nG\n#\nA B\nA F\nB G\nC F\nC G\nD B\nE A\nE B\nE F\nF C\nF G",
                    "A\nB\n#\nA B",
                    "mmmm_long_identifier_mm\nlong_identifier_2\n#\nmmmm_long_identifier_mm long_identifier_2",
                    "aap\nnoot\nmies\nwim\nzus\njet\nteun\nvuur\ngijs\nlam\nkees\nbok\nweide\ndoes\nhok\nduif\nschapen"
            }) {
                models.add(new MultiMapGraphModel(convertTGF(new BufferedReader(new StringReader(tgf)))));
                model = models.get(0);
            }
        } catch (IOException e) {
            // Impossible (because of StringReader)
        }
    }

    @Override
    public void start(Stage primaryStage) {
        GraphPane graphPane = new GraphPane(model);
        graphPane.setPrefSize(600, 400);
        graphPane.setPannable(true);
        graphPane.setAutoSizeVertices(true);

        Button circleLayoutButton = new Button("circle layout");
        circleLayoutButton.setOnAction(event -> graphPane.layoutGraph(new SimpleCircleLayout()));

        Button gridLayoutButton = new Button("grid layout");
        gridLayoutButton.setOnAction(event -> graphPane.layoutGraph(new GridLayout()));

        Button zoomOutButton = new Button("zoom out");
        zoomOutButton.setOnAction(e -> graphPane.zoomOut());
        Button zoom0Button = new Button("reset zoom");
        zoom0Button.setOnAction(e -> graphPane.zoomReset());
        Button zoomInButton = new Button("zoom in");
        zoomInButton.setOnAction(e -> graphPane.zoomIn());

        Button saveAsPngButton = new Button("save as png");
        saveAsPngButton.setOnAction(event -> {
            File file = null;
            try {
                FileChooser fileChooser = new FileChooser();
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG File", "*.png"));
                file = fileChooser.showSaveDialog(primaryStage);
                if (file != null)
                    graphPane.saveAsPng(file);
            } catch (IOException e) {
                System.err.println("Writing file " + file + " failed.");
            }
        });

        Button nextModel = new Button("next model");
        nextModel.setOnAction(event -> graphPane.setModel(models.get(++modelIndex % models.size())));

        HBox buttonPane = new HBox(10.0);
        buttonPane.setPadding(new Insets(3));
        buttonPane.getChildren().addAll(circleLayoutButton, gridLayoutButton, zoomOutButton, zoom0Button, zoomInButton, saveAsPngButton, nextModel);

        BorderPane borderPane = new BorderPane();
        borderPane.setBottom(buttonPane);
        borderPane.setCenter(graphPane);

        Scene scene = new Scene(borderPane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("grafx - a simple JavaFX library for graph visualisation");
        primaryStage.show();
    }

    public static Map<String, List<String>> convertTGF(BufferedReader input) throws IOException {
        Map<String, List<String>> nodeMap = new HashMap<>();
        Map<String, String> nodeLabels = new HashMap<>();

        String line;
        while ((line = input.readLine()) != null && ! line.startsWith("#")) {
            line = line.trim();
            int space = line.indexOf(' ');
            if (space < 0) space = line.length();
            String id = line.substring(0, space);
            String label = "";
            if (line.length() > space) {
                label = line.substring(space + 1).trim();
            }
            if (label.equals("")) {
                label = id;
            }
            nodeLabels.put(id, label);
            nodeMap.put(id, new ArrayList<>());
        }
        while ((line = input.readLine()) != null) {
            int space = line.indexOf(' ');
            String from = line.substring(0, space);
            int space2 = line.indexOf(' ', space+1);
            if (space2 < 0) space2 = line.length();
            String to = line.substring(space+1, space2);
            nodeMap.get(from).add(to);
        }
        return nodeMap;
    }
}

