package grafx.demo;

import grafx.GraphModel;
import grafx.ModelChangeListener;

import java.awt.Point;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class MultiMapGraphModel implements GraphModel<String, String> {

    private final Map<String, List<String>> nodeMap;

    /**
     * @param data   Maps nodes (represented by String's) to connected nodes.
     */
    public MultiMapGraphModel(Map<String, List<String>> data) {
        nodeMap = data;
    }

    @Override
    public Collection<String> getVertices() {
        return nodeMap.keySet();
    }

    @Override
    public Collection<String> getOutgoingEdges(String vertex) {
        // For edges, we simply return the target vertex's string representation.
        return nodeMap.get(vertex);
    }

    @Override
    public String getEndVertex(String edge) {
        // Edge representation is identical to target vertex.
        return edge;
    }

    @Override
    public Point getLocation(String Vertex) {
        return null;
    }

    @Override
    public void setLocation(String vertex, Point location) {}

    @Override
    public void addListener(ModelChangeListener<String, String> listener) {}

    @Override
    public void removeListener(ModelChangeListener<String, String> listener) {}

    @Override
    public String getVertexToolTipText(String vertex) {
        return "Vertex '" + vertex + "'";
    }

    @Override
    public String getEdgeToolTipText(String edge) {
        return "Link to vertex '" + edge + "'";
    }
}
