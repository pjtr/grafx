package grafx;

import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.WritableImage;
import javafx.util.Duration;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.Collection;

public class GraphPane<V,E> extends ScrollPane {

    private GraphRegion<V,E> graphRegion;

    public GraphPane(GraphModel<V,E> model) {
        graphRegion = new GraphRegion(model);
        // Embed the contents in a group to let the scrollpane use the visual bounds for layout calculations.
        // Without it, zoom will not effect the scrollpane's scrolling and the graph will become partly invisible.
        Group zoomGroup = new Group(graphRegion);
        setContent(zoomGroup);
    }

    public GraphPane() {
        graphRegion = new GraphRegion();
        // Embed the contents in a group to let the scrollpane use the visual bounds for layout calculations.
        // Without it, zoom will not effect the scrollpane's scrolling and the graph will become partly invisible.
        Group zoomGroup = new Group(graphRegion);
        setContent(zoomGroup);
    }

    public void setModel(GraphModel model) {
        graphRegion.setModel(model);
    }

    public GraphModel getModel() {
        return graphRegion.getModel();
    }

    public void setAutoSizeVertices(boolean autoSize) { graphRegion.setAutoSizeVertices(autoSize); }

    public final MultipleSelectionModel<V> getVertexSelectionModel() {
        return graphRegion.getVertexSelectionModel();
    }

    public void setSelection(Collection<V> nodes) {
        graphRegion.setSelection(nodes);
        // Scroll one of the nodes into view
        // TODO: do not move when one selected node (or all nodes?) are visible
        Point2D point = graphRegion.getPosition(nodes.iterator().next());
        if (point != null) {
            double origHval = getHvalue();
            double origVval = getVvalue();
            double newHval = point.getX() / graphRegion.getWidth();
            double newVval = point.getY() / graphRegion.getHeight();
            Animation scrollTransition = new Transition() {
                {
                    setCycleDuration(Duration.millis(2000));
                }

                @Override
                protected void interpolate(double frac) {
                    GraphPane.this.setHvalue(origHval + (newHval - origHval) * frac);
                    GraphPane.this.setVvalue(origVval + (newVval - origVval) * frac);
                }
            };
            scrollTransition.play();
        }
    }

    public void layoutGraph(AutoLayout layout) {
        graphRegion.layoutGraph(layout);
    }

    public void zoomOut() {
        graphRegion.zoomOut();
    }

    public void zoomReset() {
        graphRegion.zoomReset();
    }

    public void zoomIn() {
        graphRegion.zoomIn();
    }

    public void saveAsPng(File outputFile) throws IOException {
        WritableImage image = graphRegion.snapshot(new SnapshotParameters(), null);
        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", outputFile);
    }
}
