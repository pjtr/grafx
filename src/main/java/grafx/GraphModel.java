package grafx;

import java.awt.Point;
import java.util.Collection;

public interface GraphModel<V, E> {

    Collection<V> getVertices();

    Collection<E> getOutgoingEdges(V vertex);

    V getEndVertex(E edge);

    Point getLocation(V Vertex);

    void setLocation(V vertex, Point location);

    void addListener(ModelChangeListener<V, E> listener);

    void removeListener(ModelChangeListener<V, E> listener);

    String getVertexToolTipText(V vertex);

    String getEdgeToolTipText(E edge);
}
