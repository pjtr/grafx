package grafx;

public interface ModelChangeListener<V, E> {

    public void vertexAdded(V vertex);
    public void vertexRemoved(V vertex);
}
