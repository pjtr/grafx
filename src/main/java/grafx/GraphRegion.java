package grafx;

import javafx.animation.ParallelTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


class GraphRegion<V, E> extends Region implements ModelChangeListener<V, E> {

    private static final double HEIGHT_MARGIN = 10;
    private static final double WIDTH_MARGIN = 10;
    private static final double ZOOM_DELTA = 0.9;

    private GraphModel<V, E> graphModel;
    private Map<V, Vertex<V>> vertexMap = new HashMap<>();
    private MultipleSelectionModel<V> vertexSelectionModel = new VertexSelectionModel();

    private boolean autoSize = false;
    private int defaultWidth = 100;
    private int defaultHeight = 30;
    private double zoomFactor = 1;
    private List<Vertex<V>> selection = new ArrayList<>();
    private ObservableList<V> selectedNodes = FXCollections.observableArrayList();

    private Point2D lastClick;
    private Rectangle selectionBox;
    private boolean draggingSelection;


    GraphRegion() {
        this.setOnMouseClicked(event -> {
            if (!draggingSelection)
                unSelectAll();
            else
                draggingSelection = false;
        });
        this.setOnMousePressed(event -> {
            lastClick = new Point2D(event.getX(), event.getY());
        });
        this.setOnMouseDragged(event -> {
            draggingSelection = true;
            double x = Math.min(event.getX(), lastClick.getX());
            double width = Math.abs(event.getX() - lastClick.getX());
            double y = Math.min(event.getY(), lastClick.getY());
            double height = Math.abs(event.getY() - lastClick.getY());
            if (selectionBox == null) {
                selectionBox = new Rectangle(x, y, width, height);
                selectionBox.setStroke(Color.YELLOW);
                selectionBox.setFill(new Color(1, 1, 0, 0.1));
                getChildren().add(selectionBox);
            } else {
                selectionBox.setX(x);
                selectionBox.setY(y);
                selectionBox.setWidth(width);
                selectionBox.setHeight(height);
            }
            replaceSelection(vertexMap.values().stream().filter(v
                    -> selectionBox.contains(v.getLayoutX(), v.getLayoutY())).collect(Collectors.toSet()));
        });
        this.setOnMouseReleased(event -> {
            if (draggingSelection) {
                getChildren().remove(selectionBox);
                selectionBox = null;
                draggingSelection = false;
            }
        });
    }

    GraphRegion(GraphModel model) {
        this();
        setModel(model);
    }

    // Note that setting the same model has the side-effect of re-creating the graph.
    public void setModel(GraphModel model) {
        clearGraph();
        vertexMap.clear();
        if (graphModel != null)
            graphModel.removeListener(this);

        if (model != null) {
            graphModel = model;
            createGraph();
            graphModel.addListener(this);
        }
    }

    public GraphModel getModel() {
        return graphModel;
    }

    public final MultipleSelectionModel<V> getVertexSelectionModel() {
        return vertexSelectionModel;
    }

    Point2D getPosition(V node) {
        Vertex v = vertexMap.get(node);
        return v != null? new Point2D(v.getLayoutX(), v.getLayoutY()): null;
    }

    public void setAutoSizeVertices(boolean autoSize) {
        this.autoSize = autoSize;
    }

    public boolean getAutoSizeVertices() {
        return autoSize;
    }

    public void vertexAdded(V node) {
        int x = 100, y = 100;
        Point preferredLocation = graphModel.getLocation(node);
        if (preferredLocation != null) {
            x = preferredLocation.x;
            y = preferredLocation.y;
        }
        else {
            graphModel.setLocation(node, new Point(x, y));
        }
        Vertex v = addGraphNode(x, y, node);
        vertexMap.put(node, v);
        graphModel.getOutgoingEdges(node).forEach(edge -> {
                    addGraphConnect(vertexMap.get(node), edge, vertexMap.get(graphModel.getEndVertex(edge)));
                });
        graphModel.getVertices().forEach(otherVertex -> {   // TODO: linear search not optimal, maybe change model?
            graphModel.getOutgoingEdges(otherVertex).forEach(edge -> {
                if (graphModel.getEndVertex(edge) == node)
                    addGraphConnect(vertexMap.get(otherVertex), edge, vertexMap.get(node));
            });
        });
    }

    public void vertexRemoved(V node) {
        if (vertexMap.containsKey(node)) {
            Vertex vertex = vertexMap.get(node);
            getChildren().remove(vertex);
            ((NodeData) vertex.getUserData()).incomingConnections.forEach(e -> getChildren().remove(e));
            ((NodeData) vertex.getUserData()).outGoingConnections.forEach(e -> getChildren().remove(e));
            vertexMap.remove(node);
        }
    }

    private void createGraph() {
        int x = 100;
        int y = 100;
        for (V node: graphModel.getVertices()) {
            Point preferredLocation = graphModel.getLocation(node);
            if (preferredLocation != null) {
                x = preferredLocation.x;
                y = preferredLocation.y;
            }
            else {
                graphModel.setLocation(node, new Point(x, y));
            }
            Vertex v = addGraphNode(x, y, node);
            vertexMap.put(node, v);
            x += 70;
            y += 90;
        }
        vertexMap.keySet().forEach(node -> {
            graphModel.getOutgoingEdges(node).forEach(edge -> {
                addGraphConnect(vertexMap.get(node), edge, vertexMap.get(graphModel.getEndVertex(edge)));
            });
        });
    }

    private Vertex addGraphNode(int x, int y, V node) {
        Vertex v;
        if (autoSize)
            v = new Vertex(this, node, node.toString(), x, y);
        else
            v = new Vertex(this, node, node.toString(), x, y, defaultWidth, defaultHeight);
        if (graphModel.getVertexToolTipText(node) != null) {
            Tooltip tooltip = new Tooltip(graphModel.getVertexToolTipText(node));
            Tooltip.install(v, tooltip);
        }
        getChildren().add(v);
        recomputeSize(v);
        return v;
    }

    private Edge<E> addGraphConnect(Vertex from, E arc, Vertex to) {
        if (from == null || to == null) {
            // One of the vertices is not (yet) part of the graph, so ignore.
            // If it is added later, this edge will be added to
            return null;
        }

        Edge edge = new Edge(from, to, arc);
        if (graphModel.getEdgeToolTipText(arc) != null) {
            Tooltip tooltip = new Tooltip(graphModel.getEdgeToolTipText(arc));
            Tooltip.install(edge, tooltip);
        }
        getChildren().add(edge);
        edge.toBack();
        from.toFront();
        to.toFront();

        ((NodeData) from.getUserData()).outGoingConnections.add(edge);
        ((NodeData) to.getUserData()).incomingConnections.add(edge);

        return edge;
    }

    private void clearGraph() {
        getChildren().clear();
    }

    void addToSelection(Vertex<V> graphNode) {
        selection.add(graphNode);
        selectedNodes.add(graphNode.getUserNode());
        graphNode.setSelected();
    }

    void setSelection(Vertex<V> graphNode) {
        unSelectAll();
        selection.add(graphNode);
        selectedNodes.add(graphNode.getUserNode());
        graphNode.setSelected();
    }

    private void replaceSelection(Set<Vertex<V>> newSelection) {
        Set<Vertex<V>> oldSelection = new HashSet<Vertex<V>>(selection);
        // Make oldSelection the items that are removed from the collection
        oldSelection.removeAll(newSelection);
        // Make newSelection the items that are newly added
        newSelection.removeAll(selection);

        if (! oldSelection.isEmpty()) {
            selection.removeAll(oldSelection);
            for (Vertex<V> v: oldSelection) v.setUnselected();
            selectedNodes.removeAll(oldSelection.stream().map(v -> v.getUserNode()).collect(Collectors.toList()));
        }
        if (! newSelection.isEmpty()) {
            selection.addAll(newSelection);
            for (Vertex<V> v : newSelection) v.setSelected();
            selectedNodes.addAll(newSelection.stream().map(v -> v.getUserNode()).collect(Collectors.toList()));
        }
    }

    void setSelection(Collection nodes) {
        setSelection(nodes.stream().map(node -> vertexMap.get(node)).filter(e -> e != null));
    }

    private void setSelection(Stream<Vertex<V>> selected) {
        unSelectAll();
        selectedNodes.setAll(selected.peek(v -> {
            selection.add(v);
            v.setSelected();
        }).map(v -> v.getUserNode()).collect(Collectors.toList()));
    }

    void unSelectAll() {
        selection.forEach(v -> {
            v.setUnselected();
        });
        selection.clear();
        selectedNodes.clear();
    }

    public void moveSelection(double dx, double dy) {
        selection.forEach(v -> {
            v.move(dx, dy);
            graphModel.setLocation(v.getUserNode(), new Point((int) v.getLayoutX(), (int) v.getLayoutY()));
            recomputeSize(v);
        });
    }

    void recomputeSize(Vertex v) {
        double maxX = v.getLayoutX() + v.getWidth() + WIDTH_MARGIN;
        double maxY = v.getLayoutY() + v.getHeight() + HEIGHT_MARGIN;
        if (getPrefWidth() < maxX)
            setPrefWidth(maxX);
        if (getPrefHeight() < maxY)
            setPrefHeight(maxY);
    }

    public void layoutGraph(AutoLayout layout) {
        List<Vertex<V>> vertices = new ArrayList<>(this.vertexMap.values());
        List<Point2D> positions = layout.computePositions(vertices);
        ParallelTransition parallelTransition = new ParallelTransition();
        for (int i = 0; i < vertices.size(); i++) {
            TranslateVertexTransition transition = new TranslateVertexTransition(Duration.millis(2000), vertices.get(i));
            transition.setToX(positions.get(i).getX());
            transition.setToY(positions.get(i).getY());
            parallelTransition.getChildren().add(transition);
            graphModel.setLocation(vertices.get(i).getUserNode(), new Point((int) positions.get(i).getX(), (int) positions.get(i).getY()));
        }
        parallelTransition.play();
    }

    public void zoomOut() {
        zoomFactor *= ZOOM_DELTA;
        this.setScaleX(zoomFactor);
        this.setScaleY(zoomFactor);
    }
    public void zoomReset() {
        zoomFactor = 1;
        this.setScaleX(zoomFactor);
        this.setScaleY(zoomFactor);
    }

    public void zoomIn() {
        zoomFactor /= ZOOM_DELTA;
        this.setScaleX(zoomFactor);
        this.setScaleY(zoomFactor);
    }

    static class NodeData {
        List<Edge> incomingConnections;
        List<Edge> outGoingConnections;

        NodeData() {
            incomingConnections = new ArrayList<>();
            outGoingConnections = new ArrayList<>();
        }
    }

    class VertexSelectionModel extends MultipleSelectionModel<V> {

        @Override
        public ObservableList<Integer> getSelectedIndices() {
            throw new UnsupportedOperationException("index operations not supported");
        }

        @Override
        public ObservableList<V> getSelectedItems() {
            return selectedNodes;
        }

        @Override
        public void selectIndices(int index, int... indices) {
            throw new UnsupportedOperationException("index operations not supported");
        }

        @Override
        public void selectAll() {
            setSelection(vertexMap.values().stream());
        }

        @Override
        public void clearAndSelect(int index) {
            throw new UnsupportedOperationException("index operations not supported");
        }

        @Override
        public void select(int index) {
            throw new UnsupportedOperationException("index operations not supported");
        }

        @Override
        public void select(V node) {
            if (vertexMap.containsKey(node))
                addToSelection(vertexMap.get(node));
        }

        @Override
        public void clearSelection(int index) {
            throw new UnsupportedOperationException("index operations not supported");
        }

        @Override
        public void clearSelection() {
            unSelectAll();
        }

        @Override
        public boolean isSelected(int index) {
            throw new UnsupportedOperationException("index operations not supported");
        }

        @Override
        public boolean isEmpty() {
            return selectedNodes.isEmpty();
        }

        @Override
        public void selectPrevious() {
            throw new UnsupportedOperationException("index operations not supported");
        }

        @Override
        public void selectNext() {
            throw new UnsupportedOperationException("index operations not supported");
        }

        @Override
        public void selectFirst() {
            throw new UnsupportedOperationException("index operations not supported");
        }

        @Override
        public void selectLast() {
            throw new UnsupportedOperationException("index operations not supported");
        }
    }
}

