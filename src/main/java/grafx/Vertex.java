package grafx;

import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.util.stream.Stream;

/**
 *
 * @param <U>   Type of user node (the V from the GraphModel<V,E>)
 */
class Vertex<U> extends Group {

    private static final double RESIZE_MARGIN = 3;
    private static final int MIN_WIDTH = 30;
    private static final double LABEL_MARGIN = 10;
    private static final double LABEL_MARGIN_Y = 10;

    private final GraphRegion graph;
    private final U userNode;
    private final Rectangle vertexRect;
    private final Label vertexLabel;

    private Point2D lastPositionDuringMove;

    private boolean prepareResize;
    private boolean doResize;
    private int resizeDirection;
    private boolean isSelected;

    Vertex(GraphRegion graph, U node, String label, int x, int y, int width, int height) {
        this(graph, node, label);

        Dimension2D labelDim = computeLabelSize(label);
        vertexLabel.setLayoutY(labelDim.getHeight() / -2);
        vertexRect.setWidth(width);
        vertexRect.setHeight(height);

        postConstruct(x, y, width, height);
    }

    /**
     * Creates vertex with 'auto size' property: size is derived from label size.
     * @param graph
     * @param label
     * @param x
     * @param y
     */
    Vertex(GraphRegion graph, U node, String label, int x, int y) {
        this(graph, node, label);

        //vertexLabel.setFont(javafx.scene.text.Font.font(20));
        Dimension2D labelDim = computeLabelSize(label);
        vertexLabel.setLayoutY(labelDim.getHeight() / -2);
        int width = (int) Math.max(labelDim.getWidth() + LABEL_MARGIN, MIN_WIDTH);
        int height = (int) (labelDim.getHeight() + LABEL_MARGIN_Y);
        vertexRect.setWidth(width);
        vertexRect.setHeight(height);

        postConstruct(x, y, width, height);
    }

    private Vertex(GraphRegion graph, U node, String label) {
        this.graph = graph;
        this.userNode = node;
        this.vertexLabel = new Label(label);
        this.vertexRect = new Rectangle();

        this.setOnMouseEntered(event -> {
            if (isSelected()) {
                this.setCursor(Cursor.HAND);
            }
        });

        this.setOnMouseExited(event -> {
            this.setCursor(Cursor.DEFAULT);
        });

        this.setOnMouseMoved(event -> {
            if (inResizeZone(event)) {
                prepareResize = true;
                this.setCursor(Cursor.E_RESIZE);
            } else if (isSelected()) {
                this.setCursor(Cursor.CLOSED_HAND);
                prepareResize = false;
            } else if (prepareResize) {
                prepareResize = false;
                this.setCursor(Cursor.DEFAULT);
            }
            event.consume();
        });

        this.setOnMouseClicked(event -> {
            // Eat event to avoid parent gets it.
            event.consume();
        });

        this.setOnMousePressed(event -> {
            if (prepareResize) {
                doResize = true;
                resizeDirection = (int) Math.signum(event.getX());
            } else {
                if (!isSelected()) {
                    handleSelectEvent(event);
                    this.setCursor(Cursor.CLOSED_HAND);
                }

                lastPositionDuringMove = graph.sceneToLocal(event.getSceneX(), event.getSceneY());

                if (getUserData() != null) {
                    GraphRegion.NodeData nodeData = (GraphRegion.NodeData) getUserData();
                    Stream.concat(nodeData.incomingConnections.stream(), nodeData.outGoingConnections.stream()).forEach(edge -> {
                        edge.toFront();
                    });
                }
                toFront();
            }
            event.consume();
        });

        this.setOnMouseReleased(event -> {
            prepareResize = false;
            doResize = false;
            if (!isSelected())
                setCursor(Cursor.DEFAULT);
            event.consume();
        });

        this.setOnMouseDragged(event -> {
            if (doResize) {
                if (resizeDirection * event.getX() > MIN_WIDTH / 2) {
                    vertexRect.setWidth(2 * (Math.abs(event.getX())));
                    vertexRect.setX(-1 * resizeDirection * event.getX());
                    double labelWidth = vertexRect.getWidth() - LABEL_MARGIN;
                    vertexLabel.setPrefWidth(labelWidth);
                    vertexLabel.setLayoutX(labelWidth / -2);
                    move(0, 0);  // recompute connections
                }
            } else {
                Point2D newPosition = graph.sceneToLocal(event.getSceneX(), event.getSceneY());
                Point2D diff = newPosition.subtract(lastPositionDuringMove);
                graph.moveSelection(diff.getX(), diff.getY());
                lastPositionDuringMove = graph.sceneToLocal(event.getSceneX(), event.getSceneY());
            }
            event.consume();
        });
    }

    private void postConstruct(int x, int y, int width, int height) {
        vertexLabel.setPrefWidth(width);
        vertexLabel.setLayoutX(width / -2);
        vertexLabel.setAlignment(Pos.CENTER);
        //vertexLabel.setBackground(new Background(new BackgroundFill(Color.YELLOW, null, null)));

        vertexRect.setFill(Color.AZURE);
        vertexRect.setStroke(Color.BLUE);
        vertexRect.setX(width / -2);
        vertexRect.setY(height / -2);
        double arcSize = Math.ceil(height / 5);
        vertexRect.setArcWidth(arcSize);
        vertexRect.setArcHeight(arcSize);

        this.setLayoutX(x);
        this.setLayoutY(y);
        this.getChildren().add(vertexRect);
        this.getChildren().add(vertexLabel);

        this.setUserData(new GraphRegion.NodeData());
    }

    public U getUserNode() {
        return userNode;
    }

    Dimension2D computeLabelSize(String label) {
        Text text = new Text(label);
            text.setFont(vertexLabel.getFont());
        return new Dimension2D(text.getLayoutBounds().getWidth(), text.getLayoutBounds().getHeight());
    }

    double getWidth() {
        return vertexRect.getWidth();
    }

    double getHeight() {
        return vertexRect.getHeight();
    }

    void setPosition(double x, double y) {
        setLayoutX(x);
        setLayoutY(y);
        moveConnectionPoints();
        toFront();
    }

    private boolean inResizeZone(MouseEvent event) {
        return (vertexRect.getWidth() / 2) - Math.abs(event.getX()) < RESIZE_MARGIN;
    }

    void move(double dx, double dy) {
        setLayoutX(getLayoutX() + dx);
        setLayoutY(getLayoutY() + dy);
        moveConnectionPoints();
    }

    private void moveConnectionPoints() {
        if (getUserData() != null) {
            GraphRegion.NodeData nodeData = (GraphRegion.NodeData) getUserData();
            nodeData.incomingConnections.stream().forEach( e -> {
                Point2D p = computeConnectionPoint(e);
                e.moveHead(p);
            });
            nodeData.outGoingConnections.stream().forEach(e -> {
                Point2D p = e.head().computeConnectionPoint(e);
                e.moveHead(p);
            });
        }
    }

    private void handleSelectEvent(MouseEvent event) {
        if (event.isShiftDown())
            graph.addToSelection(this);
        else
            graph.setSelection(this);
    }

    void setSelected() {
        vertexRect.setStrokeWidth(3);
        Color currentColor = (Color) vertexRect.getFill();
        vertexRect.setFill(currentColor.saturate().saturate());
        isSelected = true;
    }

    void setUnselected() {
        vertexRect.setStrokeWidth(1);
        Color currentColor = (Color) vertexRect.getFill();
        vertexRect.setFill(currentColor.desaturate().desaturate());
        isSelected = false;
    }

    boolean isSelected() {
        return isSelected;
    }

    Point2D computeConnectionPoint(Edge edge) {
        double angle = edge.angle();
        double halfWidth = vertexRect.getWidth() / 2;
        double halfHeight = vertexRect.getHeight() / 2;

        // Assume connect at top/bottom
        double dx1;
        double dy1 = halfHeight;
        if (Math.tan(angle) != 0)
            dx1 = halfHeight / Math.tan(angle);
        else {
            dx1 = halfWidth;
        }
        if (angle < 0) {
            dx1 = dx1 * -1;
            dy1 = dy1 * -1;
        }

        // Assume connect at left/right
        double dx2 = halfWidth;
        double dy2;
        dy2 = Math.tan(angle) * halfWidth;
        if (Math.abs(angle) > Math.PI / 2) {
            dx2 = -1 * dx2;
            dy2 = -1 * dy2;
        }

        // Select either 1 or 2, depending on distance to center
        boolean connectTopBottom = true;
        if (Math.abs(dx1) > halfWidth * 0.99) {
            connectTopBottom = false;
        } else if (Math.abs(dy1) > getHeight() * 1.01) {
            connectTopBottom = false;
        }

        Point2D currentLocation = new Point2D(getLayoutX(), this.getLayoutY());
        if (connectTopBottom)
            return currentLocation.subtract(new Point2D(dx1, -dy1));
        else
            return currentLocation.subtract(new Point2D(dx2, -dy2));
    }
}
