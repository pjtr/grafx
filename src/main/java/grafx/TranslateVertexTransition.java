package grafx;

import javafx.animation.Transition;
import javafx.util.Duration;

class TranslateVertexTransition extends Transition {

    private final Vertex node;
    private double fromX, fromY, toX, toY;

    TranslateVertexTransition(Duration duration, Vertex node) {
        setCycleDuration(duration);
        this.node = node;
        fromX = node.getLayoutX();
        fromY = node.getLayoutY();
    }

    void setToX(double x) {
        toX = x;
    }

    void setToY(double y) {
        toY = y;
    }

    protected void interpolate(double frac) {
        double dx = frac * (toX - fromX);
        double dy = frac * (toY - fromY);
        node.setPosition(fromX + dx, fromY + dy);
    }

}
