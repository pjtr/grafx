package grafx;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.transform.Rotate;

class Edge<E> extends Group {

    private final E userArc;
    private Line link;
    private Vertex tail;
    private Vertex head;
    private Circle connectPoint;
    private Polygon arrowHead;

    Edge(Vertex from, Vertex to, E arc) {
        userArc = arc;
        tail = from;
        head = to;
        link = new Line();
        link.startXProperty().bindBidirectional(tail.layoutXProperty());
        link.startYProperty().bindBidirectional(tail.layoutYProperty());
        link.endXProperty().bindBidirectional(head.layoutXProperty());
        link.endYProperty().bindBidirectional(head.layoutYProperty());

        Point2D connectionPoint = to.computeConnectionPoint(this);
        Circle c = new Circle(connectionPoint.getX(), connectionPoint.getY(), 3);
        connectPoint = c;
        arrowHead = createArrow(connectionPoint);

        getChildren().addAll(link, arrowHead);
    }

    public void toFront() {
        link.toFront();
        arrowHead.toFront();
        head.toFront();
    }

    Vertex head() {
        return head;
    }

    double angle() {
        double dx = link.getEndX() - link.getStartX();
        double dy = -1 * (link.getEndY() - link.getStartY());

        double angle;
        if (dx != 0) {
            return Math.atan2(dy, dx);
        }
        else {
            if (dy > 0)
                angle = Math.toRadians(90);
            else
                angle = Math.toRadians(-90);
        }
        return angle;
    }

    void moveHead(Point2D to) {
        connectPoint.setCenterX(to.getX());
        connectPoint.setCenterY(to.getY());
        arrowHead.setLayoutX(to.getX());
        arrowHead.setLayoutY(to.getY());
        arrowHead.getTransforms().clear();
        arrowHead.getTransforms().add(new Rotate(0 - Math.toDegrees(angle()), 0, 0));
    }

    private Polygon createArrow(Point2D connectionPoint) {
        Polygon polygon = new Polygon();
        polygon.getPoints().addAll(new Double[]{
                0.0, 0.0,
                -10.0, 5.0,
                -7.0, 0.0,
                -10.0, -5.0});

        polygon.setLayoutX(connectionPoint.getX());
        polygon.setLayoutY(connectionPoint.getY());
        polygon.getTransforms().add(new Rotate(0 - Math.toDegrees(angle()), 0, 0));
        return polygon;
    }

}
