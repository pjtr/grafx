package grafx;

import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GridLayout<V> implements AutoLayout<V> {

    double rowColFactor = 1.33;

    public GridLayout() {}

    public GridLayout(double rowColumnFactor) {
        this.rowColFactor = rowColumnFactor;
    }

    @Override
    public List<Point2D> computePositions(List<Vertex<V>> vertices) {
        Collections.shuffle(vertices);
        int maxWidth = (int) vertices.stream().max((a,b) -> a.getWidth() < b.getWidth()?-1:1).get().getWidth();
        int maxHeight = (int) vertices.get(0).getHeight();  // All vertices have same height.
        int count = vertices.size();
        int rows = (int) Math.ceil(Math.sqrt(count * rowColFactor));
        int startX = 100;
        int startY = 70;
        int deltaX = maxWidth + 50;
        int deltaY = maxHeight + 50;
        int currentX = startX;
        int currentY = startY - deltaY;
        int currentRowCount = 0;
        List<Point2D> result = new ArrayList<>();
        for (Vertex v: vertices) {
            if (currentRowCount == rows) {
                currentRowCount = 0;
                currentY = startY;
                currentX += deltaX;
                currentRowCount++;
            }
            else {
                currentY += deltaY;
                currentRowCount++;
            }
            result.add(new Point2D(currentX, currentY));
        }
        return result;
    }
}